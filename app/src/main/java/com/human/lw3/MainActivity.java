package com.human.lw3;

import android.animation.TypeConverter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;

public class MainActivity extends AppCompatActivity {
    GridLayout numbersGrid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numbersGrid = (GridLayout)findViewById(R.id.numbersGrid);
        numbersGrid.setColumnCount(3);
        numbersGrid.setRowCount(3);
        addButtonsToGrid(generateButtons());

    }

    private Button[] generateButtons() {
        Button[] buttons = new Button[9];
        for (int i=0; i<9; i++) {
            buttons[i] = new Button(this);
            buttons[i].setText(String.valueOf(i+1));
        }
        return buttons;
    }

    private void addButtonsToGrid(Button[] buttons) {
        for (Button button: buttons) {
            numbersGrid.addView(button);
        }
    }
}
